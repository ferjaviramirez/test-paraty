import { NgModule,CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MaterialModule } from '../material/material.module';
import { ReservaComponent } from './pages/reserva/reserva.component';



@NgModule({
  declarations: [
    ReservaComponent
  ],
  imports: [
    CommonModule,
    MaterialModule
  ],
  exports:[
    ReservaComponent
  ],
  schemas: [ CUSTOM_ELEMENTS_SCHEMA ]
})
export class ParatyModule { }
